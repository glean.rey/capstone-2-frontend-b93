let formEdit = document.querySelector("#editCourse");
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);

fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/${courseId}`)
.then(res=>res.json())
.then(data=>{
	console.log(data);
	courseName.placeholder=data.name;
	courseName.value=data.name;
	courseDescription.placeholder=data.description;
	courseDescription.value=data.description;
	coursePrice.placeholder=data.price;
	coursePrice.value=data.price;

formEdit.addEventListener("submit", (e) => {
	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/${courseId}`, {
		method: "PUT",
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(data === true){
			alert("Course successfully updated!");
			window.location.replace("./courses.html");
		} else {
			alert("Something went wrong!");
		}
	})
})
})