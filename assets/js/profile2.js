let token = localStorage.getItem("token");
let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null) {

	alert('You must login first');
	window.location.href="./login.html";

} else {

	fetch('https://floating-shelf-63249.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML = 
			`
				<div class="col-md-12">
					<section class="jumbotron my-5">		
						<h1 class="text-center">${data.firstName} ${data.lastName}</h1>
						<h5 class="text-center">${data.email}</h5>
						<h5 class="text-center">${data.mobileNo}</h5>
						<div class="col-md-2 offset-md-5">
						<a href="./editProfile.html" class="btn btn-block btn-dark text-white">Edit Profile</a>
						</div>
						<h3 class="text-center mt-5">Enrollments History</h3>
						<div id="courses"></div> 
					</section>
				</div>
			`
		let courses = document.querySelector("#courses");
		if(data.enrollments.length > 0) {
			data.enrollments.forEach(courseData => {
				console.log(courseData);
				fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/${courseData.courseId}`)
				.then(res => res.json())
				.then(data => {
					courses.innerHTML += 
						`
							<div class="card">
								<div class="card-body">
									<h5 class="card-text text-center">${data.name}</h5>
									<h5 class="card-text text-center">${courseData.enrolledOn}</h5>
									<h5 class="card-text text-center">${courseData.status}</h5>
								</div>
							</div>
						`
				})
			})
		} else {
			courses.innerHTML = `<h1 class="text-center">No courses available</h1>`
		}
	})
}