console.log(window.location.search);
let params = new URLSearchParams(window.location.search);
let userId = localStorage.getItem("id");

let courseId = params.get('courseId');
let token = localStorage.getItem('token');
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	data.enrollees.map(course => {
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		enrollContainer.innerHTML = 
			`
			<button id="enrollButton" class="btn btn-block btn-info">
				Enroll
			</button>
			`
		if (userId === course.userId) {
			enrollContainer.innerHTML = 
			`
			<h4 style="color: green; font-weight: bolder;">ENROLLED</h4>
			`
		} else {
			document.querySelector("#enrollButton").addEventListener("click", () => {
			fetch('https://floating-shelf-63249.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
					userId: userId
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (token === null){
						alert("Please Login!")
						window.location.replace("./register.html");
					} else {
						alert("Thank you for Enrolling!");
						window.location.replace("./courses.html");
					}
			})
		})
	}
	})
})




