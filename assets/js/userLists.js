let admin = localStorage.getItem("isAdmin");
let userNames = document.querySelector("#userNames")
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem("token");

	fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		if (data.enrollees.length === 0) {
			alert("No enrollees available");
			window.location.replace("./courses.html");
		} else {
					data.enrollees.map(course => {
						console.log(course);
						fetch(`https://floating-shelf-63249.herokuapp.com/api/users/getUsers`)
						.then(res => res.json())
						.then(data => {
						data.map(user =>{
							if (user._id === course.userId){
							let date = new Date(course.enrolledOn)
							userNames.innerHTML += 
							`
							<div class="card">
								<div class="card-body">
									<h4>${user.firstName} ${user.lastName}</h4>
									<h6>Contact No: ${user.mobileNo}</h6>
									<h6>Enrolled on: ${date.toDateString()}</h6>
								</div>
							</div>
							`
							}
						})
					})
				})
			}
		})




