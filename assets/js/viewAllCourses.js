let coursesContainer = document.querySelector("#coursesContainer");
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let cardEnable;
let cardDelete;



	fetch('https://floating-shelf-63249.herokuapp.com/api/courses/lists/:test')
	.then(res => res.json())
	.then(data => {
		console.log(data);

	data.forEach(data => {
		if(data.isActive === false) {
					cardEnable = 
					`
						<a href="./enableCourse.html?courseId=${data._id}" value="${data._id}" class="btn btn-success text-white btn-block enableButton">Enable
						</a>	

					`
					cardDelete = 
					`
						<a href="./hardDelete.html?courseId=${data._id}" value="${data._id}" class="btn btn-danger text-white btn-block deleteButton">Delete
						</a>
					`
					coursesContainer.innerHTML += 
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title text-center">
											${data.name}
										</h5>
										<p class="card-text text-center">
											${data.description}
										</p>
										<p class="card-text text-center">
											₱ ${data.price}
										</p>
										<h5 class="card-text text-center">
											Active: ${data.isActive}
										</h5>	
									</div>
									<div class="card-footer">
										${cardEnable}
										${cardDelete}
									</div>
								</div>
							</div>
						`
		} else {	

			cardDelete = 
			`
				<a href="./hardDelete.html?courseId=${data._id}" value="${data._id}" class="btn btn-danger text-white btn-block deleteButton">Delete
				</a>
			`
			coursesContainer.innerHTML += 
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title text-center">
											${data.name}
										</h5>
										<p class="card-text text-center">
											${data.description}
										</p>
										<p class="card-text text-center">
											₱ ${data.price}
										</p>
										<h5 class="card-text text-center">
											Active: ${data.isActive}
										</h5>	
									</div>
									<div class="card-footer">
										${cardDelete}
									</div>
								</div>
							</div>
						`
		}
	})
})



