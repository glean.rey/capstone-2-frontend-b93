let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	

	if(email === "" || password === "") {
		alert("Please input your email or password!")
	} else {
		fetch('https://floating-shelf-63249.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken);
				fetch('https://floating-shelf-63249.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);

					window.location.replace("./courses.html");
				})
			} else {
				alert("Login Failed!");
			}
		})
	}
})