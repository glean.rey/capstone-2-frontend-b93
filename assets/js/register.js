let registerForm = document.querySelector("#registerUser")
registerForm.addEventListener("submit", (e) => {
	e.preventDefault(); 

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	if((password1 !== '' && password2 !== "") && (password1 === password2) && (mobileNumber.length === 11)) {

		fetch('https://floating-shelf-63249.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-type': "application/json"
			},
			body: JSON.stringify({
				userEmail: userEmail
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === false) {
				fetch('https://floating-shelf-63249.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-type': "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						password: password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if (data === true) {
						alert("Registered Successfully");
						// Redirect to login
						window.location.replace("./login.html");
					} else {
						alert("Something went wrong!")
					}
				})
			} else {
				alert("Duplicate email found!")
			}
		})
	}
})

