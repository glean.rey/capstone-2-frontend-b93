let userEdit = document.querySelector("#editUser");
let token = localStorage.getItem("token");
let userId = localStorage.getItem('id');

fetch(`https://floating-shelf-63249.herokuapp.com/api/users/details`, {
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
})
.then(res=>res.json())
.then(data=>{
	firstName.placeholder=data.firstName;
	firstName.value=data.firstName;
	lastName.placeholder=data.lastName;
	lastName.value=data.lastName;
	userEmail.placeholder=data.email;
	userEmail.value=data.email;
	mobileNumber.placeholder=data.mobileNo;
	mobileNumber.value=data.mobileNo;

	userEdit.addEventListener("submit", (e) => {
		e.preventDefault();

		let firstName = document.querySelector("#firstName").value;
		let lastName = document.querySelector("#lastName ").value;
		let userEmail = document.querySelector("#userEmail").value;
		let mobileNumber = document.querySelector("#mobileNumber").value;

		fetch(`https://floating-shelf-63249.herokuapp.com/api/users/editProfile/${userId}`, {
			method: "PUT",
			headers: {
				'Content-type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: userEmail,
				mobileNo: mobileNumber 
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				alert("User successfully updated!");
				window.location.replace("./profile2.html");
			} else {
				alert("Something went wrong!");
			}
		})
	})
})
