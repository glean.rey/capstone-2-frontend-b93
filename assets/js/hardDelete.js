let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);

fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/delete/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-type': 'application/json'
	},
})
.then(res => res.json())
.then(data => {
	console.log(data);

	if(data === 0){
		alert("Something went wrong!");
		} else {
			alert("Course has been deleted on the database!");
			window.location.replace("./courses.html");
		}
})
