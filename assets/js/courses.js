let admin = localStorage.getItem("isAdmin");
let cardFooter;
let adminText = document.querySelector("#adminTxt");
let modalButton = document.querySelector("#adminButton");
let userId = localStorage.getItem('id');
let token = localStorage.getItem("token");

if (admin === "false" || !admin) {
	modalButton.innerHTML = null;

	fetch('https://floating-shelf-63249.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		data.map(course => {
					cardFooter = 
					`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">Select Course
					</a>
					`
					document.querySelector("#coursesContainer").innerHTML +=
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body" style="min-height: 25vh;">
									<h5 class="card-title text-center">
										${course.name}
									</h5>
									<p class="card-text text-center">
										${course.description}
									</p>
									<p class="card-text text-center">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
			})
	 	})
} else {

	fetch('https://floating-shelf-63249.herokuapp.com/api/courses/lists/:test')
	.then(res => res.json())
	.then(data => {
		console.log(data);
		let courseData;
		
		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
			if(admin === "true" || admin )  {
					adminText.innerHTML = 
						`
							<div class="text-center" style="color: #00b8cc; font-family: 'Courier New', monospace;"><h3>ADMIN CONTROLS</h3></div>
						`
						modalButton.innerHTML = 
						`
							<div class="col-md-2 offset-md-10">
							<a href="./addCourse.html" class="btn btn-block btn-info text-white">Add Course</a>
							</div>
						`
					cardFooter = 
					`
						<a href="./userLists.html?courseId=${course._id}" value="${course._id}" class="btn btn-block text-white enrolleesButton" style="background-color: 	
						#001A99;">Enrollees</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">Edit
						</a>
						<a href="./enableCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enableButton">Enable
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block deleteButton">Disable Course
						</a>
						<a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">Delete
						</a>	

					`

				} if (course.isActive === true) {
					cardFooter = 
					`
						<a href="./userLists.html?courseId=${course._id}" value="${course._id}" class="btn btn-block btn-dark text-white" style="background-color: 	
						#001A99;">Enrollees</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">Edit
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block deleteButton">Disable Course
						</a>
						<a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">Delete
						</a>	

					`
				} else {
					cardFooter = 
					`
						<a href="./userLists.html?courseId=${course._id}" value="${course._id}" class="btn btn-block btn-dark text-white" style="background-color: 	
						#001A99;">Enrollees</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">Edit
						</a>
						<a href="./enableCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enableButton">Enable
						</a>
						<a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">Delete
						</a>	

					`
				}
				if (course.isActive === false) {
					return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body" style="min-height: 27vh;">
										<h5 class="card-title text-center">
											${course.name}
										</h5>
										<p class="card-text text-center">
											${course.description}
										</p>
										<p class="card-text text-center">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
									<div class="card-footer">
									<h6 class="text-center" style="color: red; font-weight: bolder;">
									Status: Inactive
									</h6>
								  </div>
								</div>
							</div>
						`
						)
				} else	{
					return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body" style="min-height: 27vh;">
										<h5 class="card-title text-center">
											${course.name}
										</h5>
										<p class="card-text text-center">
											${course.description}
										</p>
										<p class="card-text text-center">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
									<div class="card-footer">
									<h6 class="text-center" style="color: green; font-weight: bolder;">
									Status: Active
									</h6>
								  </div>
								</div>
							</div>
						`
						)	
				}
			}).join("");
		}
		document.querySelector("#coursesContainer").innerHTML = courseData;
	})
}