let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId)

	fetch(`https://floating-shelf-63249.herokuapp.com/api/courses/enable/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(data === true){
			alert("Course has been Enabled!");
			window.location.replace("./courses.html");
		} else {
			alert("Something went wrong!");
		}
	})