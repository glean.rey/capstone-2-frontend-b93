let navItems = document.querySelector('#navSession');
let registerBtn = document.querySelector("#registerBtn");
let profileBtn = document.querySelector("#profileBtn");
let userToken = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");

if(!userToken){
	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link"> Log In </a>
		</li>
		
	`
	registerBtn.innerHTML = 
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	profileBtn.innerHTML = 
	`
		<li class="nav-item">
			<a href="./profile2.html" class="nav-link"> Profile </a>
		</li>
			
	`
	registerBtn.innerHTML = 
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
		</li>
	`
} if (adminUser === "true") {
	profileBtn.innerHTML = null
	registerBtn.innerHTML = 
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
		</li>
	`
} 




